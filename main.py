from src.download_json import JsonDownload
from src.extract_data import JsonToCsv

downloader = JsonDownload()

url = "https://s3.us-west-2.amazonaws.com/secure.notion-static.com/de23874d-62c0-4d9a-b69c-726c5f10c900/dados.zip?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Credential=AKIAT73L2G45EIPT3X45%2F20221111%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20221111T163338Z&X-Amz-Expires=86400&X-Amz-Signature=db43f8f6111de1e9cda4c4037e376753a5aa968cc39d00b5439c012b7d2f591c&X-Amz-SignedHeaders=host&response-content-disposition=attachment%3B%20filename%3D%22dados.zip%22&x-id=GetObject"
downloader.download(url)

table = JsonToCsv()
table.create_dict()
a = JsonToCsv()
a.create_dict()
