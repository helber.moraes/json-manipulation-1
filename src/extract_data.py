import json
import os
import pandas as pd

class JsonToCsv:

    def dict_to_df(self, dictionary):
        df = pd.DataFrame.from_dict(dictionary)
        os.makedirs("./table/", exist_ok=True)
        df.to_csv("./table/table.csv", index=False)

    def create_dict(self):
        local_filename = "tmp.zip"
        self.bbox_dict = {"bboxes":[], "classes":[]}
        self.files_list = os.listdir('./dados')
        self.files_list.remove(local_filename)

        for i in self.files_list:
            with open(f"dados/{i}") as file:
                data = json.load(file)
            if "reviewed" in data and data['reviewed']:
                self.bbox_dict["bboxes"].append(data["bbox"])
                self.bbox_dict["classes"].append(data["class"])

        self.dict_to_df(self.bbox_dict)
