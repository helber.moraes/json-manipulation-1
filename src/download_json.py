import requests
import os
import subprocess

class JsonDownload():

    def _unzip(self):
        local_filename = "tmp.zip"
        os.makedirs("./dados/", exist_ok=True)
        retorno = subprocess.run([f'mv ./{local_filename} ./dados/{local_filename}'], shell=True)
        retorno = subprocess.run([f'unzip ./dados/{local_filename} -d ./dados/'], shell=True)

    def download(self, url):
        local_filename = "tmp.zip"
        with requests.get(url, stream=True) as r:
            r.raise_for_status()
            with open(local_filename, 'wb') as f:
                for chunk in r.iter_content(chunk_size=8192): 
                    f.write(chunk)
        
        self._unzip()
